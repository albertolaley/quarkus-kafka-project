package br.com.quarkus.kafka.consumer.example.run;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.Executors;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import br.com.quarkus.kafka.consumer.example.consumer.Consumer;
import io.quarkus.runtime.StartupEvent;

@ApplicationScoped
public class KafkaObserver {
    @Inject
    Consumer consumer;

    /**
     * Inicia o listener a partir de um observer que esta escutando 
     * uma ação do tipo StartupEvent (ação do quarkus, que será realizada na inicialização do projeto)
     *  quando isso ocorre, nosos método
     * listen será chamado
     */
    void onStart(@Observes StartupEvent ev) {
        listen();
    }
    void listen() {
        //props propriedades configuradas com servidor e tipo de chave e valor que serão recebidos
        Properties props = new Properties();
        props.put("group.id", "example");
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, String> consumerKafka = new KafkaConsumer<>(props);

        // nome do topico que iremos ler
        consumerKafka.subscribe(Arrays.asList("kafka-example-topic"));

        //while simples para realizar a leitura no tópico, assim sendo passando o valor ou uma lista deles para serem lidos
        while (true) {
            ConsumerRecords<String, String> records = consumerKafka.poll(Duration.ofSeconds(1000));
            for (ConsumerRecord<String, String> record : records)
                consumer.onReceiveMessage(record.value());
        }
    }

}