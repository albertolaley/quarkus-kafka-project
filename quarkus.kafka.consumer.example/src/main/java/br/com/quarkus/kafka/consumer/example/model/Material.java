package br.com.quarkus.kafka.consumer.example.model;

public class Material {
    private String id;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Material(String id, String description) {
        this.id = id;
        this.description = description;
    }
}